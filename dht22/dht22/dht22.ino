#include <Arduino.h>
#include <DHT.h>

#define DHTPIN 2  //DHT 
#define DHTTYPE DHT22  //DHT 

int TIME_DELAY = 10000;

DHT dht(DHTPIN, DHTTYPE); 

struct dht_med{
  float tem;
  float hum;
};





void setup() {
    Serial.begin(9600);
    pinMode(DHTPIN, INPUT);
  // put your setup code here, to run once:

}

void loop() {
  dht_med dht_measure = dht_sensor();
  float tem_med = dht_measure.tem;
  float hum_med = dht_measure.hum;
  Serial.print("TEMP: " );
  Serial.println(tem_med);
  Serial.print("HUM: ");  
  Serial.println(hum_med);
  delay(TIME_DELAY);
}


dht_med dht_sensor(){

  dht_med measure; //define struct dht_med

  measure.hum = dht.readHumidity();
  measure.tem = dht.readTemperature();
  if (isnan(measure.hum) || isnan(measure.tem)){
    Serial.println();
    Serial.print("Error DHT:");
    Serial.println();    
  }
  return measure;
}
