
# EndoTaller

EndoTaller es un ciclo de talleres y aprendizaje sobre como armar un kit de HuerTechno, crear nuevas funcionalidades, levantar y cerrar issues. 

El taller tiene modalidad teórico/práctico por lo que al comienzo se dan introducción a los contenidos que luego se aplicarán para programar el kit. 

No es necesario que tengas conocimientos previos de programación o domotica, puesto que **HuerTechno** pretende ser usado por cualquier persona interesada.

Nos reuniremos todos los domingos en R'lyeh HackLab y a continuación más info pinchando en los encuentros.


# Encuentros

- [Primer encuentro](https://0xacab.org/hueretechno/endo-talleres/tree/01-git-arduino): Introducción a edición de documentos con Git y primeros pasos con arduino.
- - [Segundo encuentro](https://0xacab.org/hueretechno/endo-talleres/) : En construcción
