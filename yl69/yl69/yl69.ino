#include <DHT.h>

#define DHTPIN 2  //DHT 
#define DHTTYPE DHT22  //DHT 
#define HUM_SOIL_PIN A0

int TIME_DELAY = 10000;

DHT dht(DHTPIN, DHTTYPE); 

struct dht_med{
  float tem;
  float hum;
};


dht_med dht_measure;
//int HUM_SOIL_PIN = A0; // yl-69
float hum_soil;



void setup() {
    Serial.begin(9600);
    pinMode(DHTPIN, INPUT);
  // put your setup code here, to run once:
  
  // put your setup code here, to run once:

}

void loop() {

  dht_measure = dht_sensor();
  float tem_med = dht_measure.tem;
  float hum_med = dht_measure.hum;
  float hum_soil = yl69_sensor();
  Serial.print("TEMP: " );
  Serial.println(tem_med);
  Serial.print("HUM: ");  
  Serial.println(hum_med);
  Serial.print("HUM_SUELO: ");  
  Serial.println(hum_soil);
  delay(TIME_DELAY);
}


dht_med dht_sensor(){

  dht_med measure; //define struct dht_med

  measure.hum = dht.readHumidity();
  measure.tem = dht.readTemperature();
  if (isnan(measure.hum) || isnan(measure.tem)){
    Serial.println();
    Serial.print("Error DHT:");
    Serial.println();    
  }
  return measure;
}


float yl69_sensor(){

  //hum_soil = analogRead(HUM_SOIL_PIN);
  hum_soil = map(analogRead(HUM_SOIL_PIN), 383, 784, 99, 0);
  return hum_soil;
}
