***Pad del primer taller***
pad.rlab.be/p/EndoTaller-01

[TALLER:](https://0xacab.org/hueretechno/endo-talleres/tree/01-git-arduino)

- Introducción al mundo de Git:
* Funcionalidad y utilidad
* Cuentas y uso

Nos creamos cuentas en GitLab y accedimos a los links "HuerTechno" y "endotalleres"

Clonamos los repositorios

Hicimos alguna modificación para luego actualizar el git

* Comandos usados:
> git status
> git clone
> git add
> git push
> git pull
> git commit

El uso del git permitirá en un futuro mantener actualizada no solo la información que vayamos aprendiendo si no además los archivos de código que usaremos (por ejemplolos .ino para la IDE)

Pasamos a la etapa de armado del kit
* Utilizando Arduino y NodeMSU(*problemas al correr los sketch Issue #)
* Copiamos los .ino del git
* Conectamos los sensores
* Ejecutamos en tiempo real y observamos los cambios

/// También miramos un poco la utilización de la Raspberry en el envío de datos al servidor para su monitoreo a través de la app... 
(Hasta acá hoy... seguiremos el próximo taller) ///

2018-07-24
